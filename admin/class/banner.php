<?php
class Banner{
    public $id_banner;

    public static function loadById($id_bann){
        $sql = new Sql(); 
        $results = $sql->select("SELECT * FROM banner WHERE id = :id",
        array(":id"=>$id_bann));
        if (count($results)>0){
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM banner ORDER BY nome");
    }

    public static function search($bann){
        $sql = new Sql();
        return $sql->select("SELECT * FROM banner WHERE nome LIKE :nome",
                array(":nome"=>"%".$bann."%"));
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_bann_insert(:nome, :email, :senha)",
            array(
                ":nome"=>$this->nome,
                ":email"=>$this->email,
                ":senha"=>$this->senha
            ));
        if(count($results)>0){
            return $results[0];
        }
    }

    public function update($_id,$_nome,$_senha){
        $sql = new Sql();
        $sql->query("UPDATE banner SET nome = :nome, senha = :senha WHERE id = :id",
            array(
                ":id"=>$_id,
                ":nome"=>$_nome,
                ":senha" => md5($_senha)
            ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM banner WHERE id = :id", 
        array(":id"=>$this->id));
    }
    // criando métodos construtores no PHP
    public function __construct($nome="",$email="",$senha=""){
        $this->nome=$nome;
        $this->email=$email;
        $this->senha = $senha;
    }

    public function __toString(){
        return json_encode(array(
            "id"=>$this->id,    
            "nome"=>$this->nome,
            "email"=>$this->email,
            "senha"=>$this->senha
                ));
    }
    
}
?>