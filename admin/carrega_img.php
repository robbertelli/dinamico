<?php
require_once('conexao.php');
// verificar se o usuário clicou no botão cadastrar
if (isset($_POST['cadastrar'])) {
    //recuperar dados dos campos do form
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $foto = $_FILES['foto'];

    if(!empty($foto['name'])){
        //largura máxima em pixels
         $largura = 1500;
        //$altura maxima em pixels
        $altura = 1800;
        //tamanho máximo em bytes
        $tamanho = 1048576;

        $error = array();
        //verifica se é uma imagem
        if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
            error[1] = "Isso não é uma imagem!";
        }
        // recuperando as dimensões
        $dimensoes = getimagesize($foto['tmp_name']);
        // var_dump($dimensoes);
        //verifica se a largura da imagem é maior que a permitida 
        if($dimensoes[0]>$largura){
            error[2] = "A largura da imagem é maior que a permitida! Não deve ultrapassar".$largura."pixels.";
        }
        if($dimensoes[1]>$altura){
            error[3] = "A altura da imagem é maior que a permitida! Não deve ultrapassar".$altura."pixels.";
        }
        //verifica se o tamanho da imagem é maior que a permitida
        if($foto['size']> $tamanho){
            error[4] = "O tamanho da imagem é maior que o permitido! Não deve ultrapassar".$tamanho."pixels";
        }
        //se não houver erros
        if (count($error)==0){
            //recupera a extensão do arquivo
            preg_match("/\.(gif|bmp|png|jpeg){1}$/i,$foto['name'],$ext");
            // gera um nome de arquivo único para a imagem
            $nome_imagem = md5(uniqid(time())).".".$ext;
            //caminho para armazenas as imagens
            $caminho_imagem = "foto/".$nome_imagem;
            // realiza o upload da imagem a partir do espaço temporario 
            move_uploaded_file($foto['tpm_name'],$caminho_imagem);
        }
        if(count($error)!=0){
            foreach ($error as $erro) {
                echo $erro."<br>"; 
            }
        }

        
    }

}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de Usuário</title>
</head>
<body>
    <h1>Novo Usuário<h1>
    <form action="<?php echo $_SERVER['PHP_SELF']?> " method="post" enctype="multipart/form-data" name="cadastro">
    Nome:<br>
    <input type="text" name="nome"><br><br>
    Email:<br>
    <input type="text" name="email"><br><br>
    foto de exibição:<br>
    <input type="text" name="foto"><br><br>
    <input type="submit" name="cadastro" value="Cadastrar">
    </form>
    
</body>
</html>